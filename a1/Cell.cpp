/**
 * \file Cell.cpp
 *
 * Implementation of the Cell.hpp interface
 */

#include "Cell.hpp"
#include "error_handler.hpp"

#include <cstring>

Cell* const nil = 0;


using namespace std;

Cell::Cell(const int i) {
  tag_m = type_int;
  int_m = i;

  ostringstream outs;
  outs << i;
  sexpr_m = outs.str();
}

Cell::Cell(const double d) {
  tag_m = type_double;
  double_m = d;
  
  ostringstream outs;
  outs << d;
  sexpr_m = outs.str();
}

Cell::Cell(const char* const s) {
  //Make a deepcopy of the parameter value to ensure that the user's variable
  //will be untouched
  char* str_cpy = new char[strlen(s)];
  strcpy(str_cpy, s);

  tag_m = type_symbol;
  symbol_m = str_cpy;
  sexpr_m = string(s);
}

Cell::Cell(Cell* const my_car, Cell* const my_cdr) {
  tag_m = type_conspair;
  conspair_m.car_m = my_car;
  conspair_m.cdr_m = my_cdr;

  if (my_cdr == nil || my_cdr->sexpr_m == "()") {
    sexpr_m = "("
      + ( (my_car == nil) ? "()" : my_car->sexpr_m )
      + ")";
  } else if (my_cdr->sexpr_m[0] == '(') {
    sexpr_m = "("
      + ( (my_car == nil) ? "()" : my_car->sexpr_m )
      + " "
      + my_cdr->sexpr_m.substr(1, my_cdr->sexpr_m.length() - 1);
  } else {
    cout << "error: can't cons a car ";
    my_car->print(cout);
    cout << " onto a cdr ";
    my_cdr->print(cout);
    cout << " that is not a list" << endl;
    exit(1);
  }
}

bool Cell::is_int() const { 
  return tag_m == type_int;
}

bool Cell::is_double() const {
  return tag_m == type_double;
}

bool Cell::is_symbol() const {
  return tag_m == type_symbol;
}

bool Cell::is_cons() const {
  return tag_m == type_conspair;
}

int Cell::get_int() const {
  if(!is_int())
    err("WrongTypeError: An integer is expected");

  return int_m;
}

double Cell::get_double() const {
  if(!is_double())
    err("WrongTypeError: A double is expected");

  return double_m;
}

string Cell::get_symbol() const {
  if(!is_symbol())
    err("WrongTypeError: Cell does not contain a string");

  return symbol_m;
}

Cell* Cell::get_car() const {
  if(!is_cons())
    err("WrongTypeError: Cell does not contain a Cons Pair");

  return conspair_m.car_m;
}

Cell* Cell::get_cdr() const {
  if(!is_cons())
    err("WrongTypeError: Cell does not contain a Cons Pair");
  
  return conspair_m.cdr_m;
}


void Cell::print(ostream& os) const {  
  os << sexpr_m;

  if(is_double())
      if(get_double() == (int) get_double())
	os << ".0";
}
