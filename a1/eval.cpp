/**
 * \file eval.cpp
 *
 * Evaluates the s-expression. Recursive approach. Each death level returns
 * its result as a cell to the upper level.
 */

#include "eval.hpp"
#include "error_handler.hpp"
#include "cons_helper.hpp"

#include <string>
#include <cmath>

// Needed for special cow functionality
#include <fstream>
#include <streambuf>

using namespace std;

/**
 * \brief Evaluates the s-expression. Recursive approach. Each deapth level returns
 * its result as a cell to the upper level.
 */
Cell* eval(Cell* const c) {

  if(c == nil)
    err("NullPointer: Head can't be NULL");

  /// returns Cell if deepest level reached
  if(!listp(c))  
    return c;
  
  Cell* curr_cell = eval(car(c));  
  Cell* curr_cons_pos = cdr(c);                 // for cons iterateration
  string curr_operator = get_symbol(curr_cell); // just for readability
  
  /// First Cell has to contain an operator
  if(!symbolp(curr_cell))
    err("WrongFormatError: No operator in front");
  
  // Later will check if number of arguments is valid
  int num_args = cons_list_size(c);

  if(curr_operator == "+") {
    if(num_args == 0)
      return make_int(0);

    double sum = 0;
    short is_double = 0;

    while(curr_cons_pos != nil) {
      curr_cell = eval(car(curr_cons_pos)); 
    
      if(doublep(curr_cell)) {
	is_double = 1;
	sum += get_double(curr_cell);
      }
      else {
	  sum += get_int(curr_cell);
      }
      
      curr_cons_pos = cdr(curr_cons_pos);
    }

    if(is_double) {
      return make_double(sum);
    }
    return make_int( (int) sum );
  }

  else if(curr_operator == "ceiling") {
    if (num_args == 0 || num_args > 1)
      err("NoOfArgumentsError: ceiling requires exactly 1 argument");

    curr_cell = eval(car(curr_cons_pos));
    
    if(intp(curr_cell))
      err("InvalidTypeError: ceiling does not accept integers as arguments");

    return make_int( (int) ceil( get_double(curr_cell)) );  
  }

  else if(curr_operator == "if") {
    if (num_args < 2 || num_args > 3) 
      err("NoOfArgumentsError: if requires at least 2 and not more than 3 arguments"); 

    curr_cell = eval(car(curr_cons_pos));
    
    // if, then
    if(symbolp(curr_cell) || ( intp(curr_cell) && get_int(curr_cell) )
       || ( doublep(curr_cell) && get_double(curr_cell) ) )
      return eval( car(cdr(curr_cons_pos)) );
    
    // else, no then
    if (cdr(cdr(curr_cons_pos)) == nil) {

      // Not specified.. so can do whatever I want, so I just.. 
      // print out a cow - Why not?
      ifstream t("cow.txt");
      string str((istreambuf_iterator<char>(t)), istreambuf_iterator<char>());
      
      return make_symbol(str.c_str());
    }

    // else
    return eval( car(cdr(cdr(curr_cons_pos))) );
  }
}
