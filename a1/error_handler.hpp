/**
 * \file error_handler.hpp
 *
 * Provides simple helper functions for dealing with errors. Use in order to 
 * make code more clear and maintainable
 */

#ifndef ERROR_HANDLER_HPP
#define ERROR_HANDLER_HPP

#include <iostream>

/**
 * \brief Simple Error Handler. Only prints error message then 
 *        exits the program, returning 1 
 * \param msg The error message to print
 */
inline void err(const char* msg) {
  std::cerr << msg << std::endl; //also prints a message since 
                                 //ERRORs without informations are a pain
  exit(1);
}

#endif
