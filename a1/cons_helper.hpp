#include "Cell.hpp"

/**
 * \brief Determines the size of the list, which is built by 
 *        nesting ConsPairs.
 *
 * \param head Head of nested the ConsPair (returns 0 if head
 *        is not a ConsPair
 *
 * Determines the size of the list, which is built by nesting 
 * ConsPairs. Universal and very useful e.g. for checking how
 * many arguments an operator/function has.
 *
 */
int cons_list_size(Cell* head) {
  if(!head->is_cons())
    return 0;

  int counter = 0;

  while(head->get_cdr() != NULL) {
    head = head->get_cdr();
    ++counter;  
  }

  return counter;
}
