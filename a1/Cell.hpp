/**
 * \mainpage COMP2012H Programming Assignment 1, Fall 2014
 *
 * 
 *
 * <h1>SPECIAL FEATURE</h1>
 * A cow (yes a cow) will be dumped, if an expression is not defined (prevents
 * user from exploiting). Try it:
 * <pre>(if 0 1)</pre> 
 *
 *
 * \author **Long Hoang**
 * \author **20149163**
 * \author **long\@mindworker.de**
 * \author **TA1**
 *
 * \date **26 Sept '14**
 *
 * Instructor: <a href="http://www.cs.ust.hk/~dekai/">Dekai Wu</a>
 *
 * TA: Karteek Addanki
 * 
 * Due: 2014.09.26 at 23:00 by CASS
 * 
 */

/**
 * \file Cell.hpp
 *
 * Encapsulates the abstract interface for a concrete class-based
 * implementation of cells for a cons list data structure.
 */

#ifndef CELL_HPP
#define CELL_HPP

#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stack>

struct ConsPair;  // forward declaration

/**
 * \class Cell
 * \brief Class Cell
 */
class Cell {
public:

  /**
   * \brief Constructor to make int cell.
   */
  Cell(int const i);

  /**
   * \brief Constructor to make double cell.
   */
  Cell(double const d);

  /**
   * \brief Constructor to make symbol cell.
   */
  Cell(const char* const s);

  /**
   * \brief Constructor to make cons cell.
   */
  Cell(Cell* const my_car, Cell* const my_cdr);

  /**
   * \brief Check if this is an int cell.
   * \return True iff this is an int cell.
   */
  bool is_int() const;

  /**
   * \brief Check if this is a double cell.
   * \return True iff this is a double cell.
   */
  bool is_double() const;

  /**
   * \brief Check if this is a symbol cell.
   * \return True iff this is a symbol cell.
   */
  bool is_symbol() const;

  /**
   * \brief Check if this is a cons cell.
   * \return True iff this is a cons cell.
   */
  bool is_cons() const;

  /**
   * \brief Accessor (error if this is not an int cell).
   * \return The value in this int cell.
   */
  int get_int() const;

  /**
   * \brief Accessor (error if this is not a double cell).
   * \return The value in this double cell.
   */
  double get_double() const;

  /**
   * \brief Accessor (error if this is not a symbol cell).
   * \return The symbol name in this symbol cell.
   */
  std::string get_symbol() const;

  /**
   * \brief Accessor (error if this is not a cons cell).
   * \return First child cell.
   */
  Cell* get_car() const;

  /**
   * \brief Accessor (error if this is not a cons cell).
   * \return Rest child cell.
   */
  Cell* get_cdr() const;

  /**
   * \brief Print the subtree rooted at this cell, in s-expression notation.
   * \param os The output stream to print to.
   */
  void print(std::ostream& os = std::cout) const;

private:

  std::string sexpr_m;

  /**
   * \struct ConsPair
   * \brief Used for implementing lists similar to those specified LISP 
            (<a href="http://en.wikipedia.org/wiki/Lisp_(programming_language)#Conses_and_lists">more</a>). 
	    It's simple, yet powerful!
  */
  struct ConsPair {
    Cell* car_m;
    Cell* cdr_m;
  };

  enum TypeTag {type_int, type_double, type_symbol, type_conspair};

  TypeTag tag_m;
  union {
    int int_m;
    double double_m;
    char* symbol_m;
    ConsPair conspair_m;
  };

};

// Reminder: cons.hpp expects nil to be defined somewhere (for this
// implementation, Cell.cpp is the logical place to define it).
// Here we promise this again, just to be safe.
extern Cell* const nil;

#endif // CELL_HPP
