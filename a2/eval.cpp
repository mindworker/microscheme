/**
 * \file eval.cpp
 *
 * Evaluates the s-expression. Recursive approach. Each death level returns
 * its result as a cell to the upper level.
 */

#include "eval.hpp"
#include "error_handler.hpp"

Cell* eval(Cell* const c) {
  if (nullp(c)) {
    err("NullPointerError: Head can't be NULL");
  }

  /// just returns Cell if deepest level reached
  /// this approach get rid of loads of if-then checks
  if (!listp(c)) {
    return c;
  }

  Cell* func = eval(car(c));
  Cell* args = cdr(c);                          

  /// The right operation/ function will be called through overwriting
  return func->call_function(args);
}
