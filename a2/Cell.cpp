/**
 * \file Cell.cpp
 *
 * Implementation of the Cell.hpp interface
 * 
 * \todo throw exceptions instead of using the current error_handler
 */

#include "Cell.hpp"
#include "functions.hpp"
#include "error_handler.hpp"

#include <cstring>

Cell* const nil = NULL;

using namespace std;


//////////////////////////////////////////
// CellABC

CellABC::~CellABC() {}

bool CellABC::is_int() const { 
  return 0;
}

bool CellABC::is_double() const {
  return 0;
}

bool CellABC::is_symbol() const {
  return 0;
}

bool CellABC::is_cons() const {
  return 0;
}

int CellABC::get_int() const {
  err("Cell does not contain an integer");
}

double CellABC::get_double() const {
  err("Cell does not contain a double");
}

double CellABC::get_numeral() const {
  err("Cell does neither contain an integer nor a double");
}

string CellABC::get_symbol() const {
  err("Cell does not contain an symbol");
}

CellABC* CellABC::get_car() const {
  err("Cell is not a ConsPair");
}

CellABC* CellABC::get_cdr() const {    
  err("Cell is not a ConsPair");
}

CellABC* CellABC::call_function(CellABC* args) const {
  err("TypeError: Cell is not a FunctionCell"); 
}



//////////////////////////////////////////
// IntCell

IntCell::IntCell(int const i) : content(i) {}

bool IntCell::is_int() const {
  return 1;
}
  
int IntCell::get_int() const {
  return content;
}

double IntCell::get_numeral() const {
  return get_int();
}

void IntCell::print(std::ostream& os) const {
  os << content;
}

//////////////////////////////////////////
// DoubleCell

DoubleCell::DoubleCell(double const d) : content(d) {}


bool DoubleCell::is_double() const {
  return 1;
}
  
double DoubleCell::get_double() const {
  return content;
}

double DoubleCell::get_numeral() const {
  return get_double();
}

void DoubleCell::print(std::ostream& os) const {
  os << content;

  /// prints .0 to indicate that it is a double
  if(content == (int) content) {
    os << ".0";
  }
}

//////////////////////////////////////////
// SynmbolCell

SymbolCell::SymbolCell(const char* const s) {
  //Make a deepcopy of the parameter value to ensure that the user's variable
  //will be untouched
  content = new char[strlen(s)];
  strcpy(content, s);
}

SymbolCell::~SymbolCell() {
  if(content == NULL) {
    delete content;
    content = NULL;
  }
}

bool SymbolCell::is_symbol() const {
  return 1;
}
  
std::string SymbolCell::get_symbol() const {
  return content;
}
  
void SymbolCell::print(std::ostream& os) const {
  os << content;
}

//////////////////////////////////////////
// ConsCell

ConsCell::ConsCell(Cell* const my_car, Cell* const my_cdr) : car(my_car), cdr(my_cdr) {}

ConsCell::~ConsCell() {
  if(car != nil) {
    delete car;
    car = NULL;
  }
  if(cdr != nil) {
    delete cdr;
    cdr = NULL;
  }
}

int ConsCell::get_list_size(Cell* head) {
  if (head == nil)
    return 0;

  int counter = 0;

  while(head != nil) {
    head = head->get_cdr();
    ++counter;  
  }

  return counter;
}

bool ConsCell::is_cons() const {
  return 1;
}
  
Cell* ConsCell::get_car() const {
  return car;
}

Cell* ConsCell::get_cdr() const {
  return cdr;
}

void ConsCell::print(std::ostream& os) const {
  string cdr_sexpr = get_sexpr(get_cdr());
  string car_sexpr = get_sexpr(get_car());
  
  /// In short it makes sure that not every bracket of cons will be shown
  /// (2 (3 (4 ()))) ==> (2 3 4) 
  if (cdr_sexpr == "()") { 
    os << "(" + car_sexpr + ")"; 
    return;
  } else if (cdr_sexpr[0] == '(') {
    os << "(" + car_sexpr
      + " "
      + cdr_sexpr.substr(1, cdr_sexpr.length() - 1);
    return;
  } else {
    err("Error: List is not correct");
  }
}

std::string ConsCell::get_sexpr(Cell* c) {
  if (c != nil) {
    ostringstream outs;

    /// recursive approach to print out ConsPair Lists
    c->print(outs);

    return outs.str();
  }
  else {
    return "()";
  }
}



//////////////////////////////////////////
// ArithmeticCell

ArithmeticCell::ArithmeticCell(const char* const s) : SymbolCell(s) {};

bool ArithmeticCell::is_arithmetic(std::string s) {
  if (s == "+" || s == "-" || s == "/" || s == "*") {
    return 1;
  }
  return 0;
}

Cell* ArithmeticCell::get_identity() const {
  std::string op = get_symbol();
    
  if (op == "+") {
    return (Cell*) new IntCell(0);
  }
  else if (op == "*") {
    return (Cell*) new IntCell(1);
  }
  else {
    err("NoOfArgumentsError: - and / cannot have zero arguments!");
  }
}

Cell* ArithmeticCell::call_function(Cell* args) const{
  int num_args = ConsCell::get_list_size(args);

  if (num_args == 0) {
    return get_identity();
  }
  else if (num_args == 1) {
    Cell* argument = eval(car(args));
    return calculate(argument);
  }
  
  /// calls a generalized function which calls itself recursively
  /// this pointer passed to provide the arithmetic operator
  return arithmetic_instruction(this, args); 

}

Cell* ArithmeticCell::calculate(Cell* c) const {
  std::string op = get_symbol();
    
  double num = c->get_numeral();
    
  if (op == "-") {
    num = -1*num;
  } 
  else if (op == "/") {
    if (num == 0) {
      err("NullError: Can not devide by zero");
    }
    num = 1/num;
  }

  /// Nothing to do for + and * operation
  
  if (c->is_double()) {
    return (Cell*) new DoubleCell(num);
  }
  return (Cell*) new IntCell((int) num);
    
}

Cell* ArithmeticCell::calculate(Cell* c1, Cell* c2) const {
  std::string op = get_symbol();
  double result = 0;
    
  double num1 = c1->get_numeral();
  double num2 = c2->get_numeral();

  if (op == "+") {
    result = num1 + num2;
  }
  else if (op == "-") {
    result = num1 - num2;
  }
  else if (op == "*") {
    result = num1 * num2;
  }
  else if (op == "/") {
    if (num2 == 0) {
      err("NullError: Can not devide by zero");
    }
    result = num1 / num2;
  }

  if (c1->is_double() || c2->is_double()) {
    return (Cell*) new DoubleCell(result);
  }
  return (Cell*) new IntCell((int)result);

}


//////////////////////////////////////////
// FunctionCell

FunctionCell::FunctionCell(const char* const s) : SymbolCell(s) {};

bool FunctionCell::is_function(string s) {
  /// \todo these things should be replaced with std::map later
  if (s == "ceiling" || s == "floor"
      || s == "if"
      || s == "quote"
      || s == "cons" || s == "car" || s == "cdr"
      || s == "nullp") {
    return 1;
  }

  return 0;
}

Cell* FunctionCell::call_function(Cell* args) const {
  if(args == nil) {
     string msg = "NoOfArgumentsError: "
      + std::string(get_symbol())                  // provides function name for
      + " cannot be called without any argument";  // backtracking bugs
    err(msg.c_str());
  }
  
  string fname = get_symbol();
  
  /// in some cases this pointer is given to the program in order to give the
  /// function more information. E.g. for generalised error_handlers it can
  /// dump a simple backtrace

  /// \todo use HashMap as soon as we are allowed to use templates!
  if (fname == "ceiling") {
    return ceiling_func(this, args);
  }
  
  if (fname == "floor") {
    return floor_func(this, args);
  }
 
  if (fname == "if") {
    return if_branching_func(args);
  }

  if (fname == "quote") {
    return quote_func(args);
  }

  if (fname == "cons") {
    return cons_func(args);
  }

  if (fname == "car") {
    return car_func(this, args);
  }

  if (fname == "cdr") {
    return cdr_func(this, args);
  }

  if (fname == "nullp") {
    return nullp_func(this, args);
  }
}
