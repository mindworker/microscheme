#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <cmath>

#include "Cell.hpp"
#include "cons.hpp"
#include "eval.hpp"

#include "error_handler.hpp"

////////////////////////////////////////////////////////////////////////////////
/// Helpers

/**
 * \brief Since most of the functions take only one argument, a small helper for
 *        better reuse of code is provided
 */
Cell* single_argument_eval(const SymbolCell* func, Cell* args) {
  if (ConsCell::get_list_size(args) != 1) {  
    string msg = "NoOfArgumentsError: "
      + std::string(func->get_symbol())      // provides function name for
      + " accepts exactly 1 argument";       // backtracking bugs
    err(msg.c_str());
  }

  return eval(car(args));
}


////////////////////////////////////////////////////////////////////////////////
/// Actual implementations of *_func functions

/**
 * \brief does arithmetic calculations recursively, until reaches terminating
 *        nil Cell. This approach enables the ability to run operations on 
 *        unlimited chained operators
 */
Cell* arithmetic_instruction(const ArithmeticCell* head, Cell* pos) {
  if (cdr(pos) == nil) {
    return eval(car(pos));
  }

  Cell* current_cell = eval(car(pos)); 
  Cell* remaining_cells = arithmetic_instruction(head, cdr(pos));
  
  return head->calculate(current_cell, remaining_cells);  
}

/**
 * \brief Implements the ceiling function, using cmath for convenience
 * \param args expects a single ConsCell with DoubleCell as car
 */
Cell* ceiling_func(const FunctionCell* func, Cell* args) {
  Cell* argument_cell = single_argument_eval(func, args);
  int res = (int) ceil(get_double(argument_cell));
  
  return make_int(res);
}

/**
 * \brief Implements the floor function, using cmath for convenience
 * \param Cell*args expects a single ConsCell with DoubleCell as car
 */
Cell* floor_func(const FunctionCell* func, Cell* args) {
  Cell* argument_cell = single_argument_eval(func, args);
  int res = (int) floor(get_double(argument_cell));
  
  return make_int(res);
}

/**
 * \brief Prevents eval to evaluate the s-expression. Basically returns the 
 *        s-expression as a SymbolCell
 * \return Cell* returns a SymbolCell or Nil if args is nil
 */
Cell* quote_func(Cell* args) {
  if (ConsCell::get_list_size(args) != 1) {                  
    err("NoOfArgumentError: quote only takes 1 argument");  /// according to csi
  }
  
  return car(args);
}

/**
 * \brief Creates a ConsPair with car and cdr similar to those of LISP
 * \param args expects a cons list of size 2 containing the car and then cdr in
 *        that order.
 */
Cell* cons_func(Cell* args) {
  if (ConsCell::get_list_size(args) != 2) {
    err("NoOfArgumentsError: cons only accepts exactly 2 arguments");
  }
  
  // For readability create tmp vars
  Cell* my_car = eval(car(args));
  Cell* my_cdr = eval(car(cdr(args)));

  if (!listp(my_cdr)) {
    err("IncorrectListError: Are you trying to use dotted pairs?");
  }

  return cons(my_car, my_cdr);
}

/**
 * \brief Exposes car from cons.hpp, also evaluates sub-trees
 * \param Cell* expects a single ConsCell
 */

Cell* car_func(const FunctionCell* func, Cell* args) {
 Cell* argument_cell = single_argument_eval(func, args);
 return car(argument_cell);
}

/**
 * \brief Exposes cdr from cons.hpp, also evaluates sub-trees
 * \param Cell* expects a single ConsCell
 */
Cell* cdr_func(const FunctionCell* func, Cell* args) {
 Cell* argument_cell = single_argument_eval(func, args);
 return cdr(argument_cell);
}

/**
 * \brief Exposes nullp from cons.hpp, also evaluates sub-trees
 * \param Cell* expects a single ConsCell
 * \return an IntCell containing 0 or 1 indicating true or false
 */
Cell* nullp_func(const FunctionCell* func, Cell* args) {
 Cell* argument_cell = single_argument_eval(func, args);

 if (nullp(argument_cell)) {
   return make_int(1);
 }
 return make_int(0);
}

/**
 * \brief If-then branching in form of a callable function. Evaluates only relevant 
          sub-trees. <b>Special Cow Feature!</b>
 * \param Cell* expects a ConsCell with two or three arguments
 */

Cell* if_branching_func(Cell* args) {
  int num_args = ConsCell::get_list_size(args);
  if (num_args < 2 || num_args > 3) {
    err("NoOfArgumentsError: if requires at least 2 and not more than 3 arguments"); 
  }

  Cell* curr_cell = eval(car(args));
    
  // if, then
  if (symbolp(curr_cell) || ( intp(curr_cell) && get_int(curr_cell) )
     || ( doublep(curr_cell) && get_double(curr_cell) ) )
    return eval( car(cdr(args)) );
    
  // else, no then
  if (cdr(cdr(args)) == nil) {

    // Not specified.. so can do whatever I want, so I just.. 
    // print out a cow - Why not?
    ifstream t("cow.txt");
    string str((istreambuf_iterator<char>(t)), istreambuf_iterator<char>());
      
    return make_symbol(str.c_str());
  }

  // else
  return eval( car(cdr(cdr(args))) );
} 

#endif
