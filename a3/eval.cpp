/**
 * \file eval.cpp
 *
 * Evaluates the s-expression. Recursive approach. Each death level returns
 * its result as a cell to the upper level.
 */

#include "eval.hpp"

#include <stdexcept>

/**
 * \brief Eval function just for the first element - or in other words for 
 *        cells which can call functions
 */
Cell* eval_func(Cell* const c) {
  if (!listp(c)) {
    if (symbolp(c)) {
      if (!FunctionCell::is_function(get_symbol(c)) 
	  && !ArithmeticCell::is_arithmetic(get_symbol(c))) {
	throw runtime_error("No operator/ function in front");
      }
    }
    return c;
  }
  
  /// in case for nested cases such as ((quote if) 1 0)
  return eval(c);
}

Cell* eval(Cell* const c) {

  /// just returns Cell if deepest level reached
  /// this approach gets rid of loads of if-then checks
  if (!listp(c)) {
    if (symbolp(c)) {
      	return c->get_definition();
    }
    return c;
  }

  Cell* func = eval_func(car(c));
  Cell* args = cdr(c);
  
  try {
    /// The right operation/ function will be called through overwriting
    return func->call_function(args);
  }
  catch (std::runtime_error) {
    if (!nullp(func)) {
      delete func;
    }

    throw;    /// re-throw the exceptions after cleaning up memory
  }
}
