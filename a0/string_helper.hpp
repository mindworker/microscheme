/**
 * \file string_helper.hpp
 *
 * Provides some helper functions for dealing with cstrings, since <cstring>
 * is not allowed
 */

#ifndef STRING_HELPER_HPP
#define STRING_HELPER_HPP

#include <iostream>

/**
 * \brief returns the lenght of the string (including null terminator)
 */
inline int string_lenght(const char* str) {
  int i, count = 0;

  while(*str++ != '\0') {
    count++;
  }

return ++count; // one more in order to include null terminator
}

/**
 * \brief Deep copy a string
 * \param dest Destination (copy to)
 * \param src Source (copy from)
 */
inline void deep_string_copy(char *dest, const char *src) {

  while(*src != '\0') {
    *dest++ = *src++;
  }
  dest ='\0';

}

#endif //STRING_HELPER_HPP
