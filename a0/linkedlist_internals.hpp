/**
 * \file linkedlist_internals.hpp
 *
 * Encapsulates an abstract interface layer for a cons list ADT,
 * without using member functions. Makes no assumptions about what
 * kind of concrete type Cell will be defined to be.
 */

#ifndef LINKEDLIST_INTERNALS_HPP
#define LINKEDLIST_INTERNALS_HPP

#include "Node.hpp"
#include "Cell.hpp"
#include "string_helper.hpp"

#include <iostream>
#include <cstdlib>


/**
 * \brief Simple Error Handler. Only prints error message then exits the program, returning 1 
 * \param msg The error message to print
 */
inline void err(const char* msg) {
  std::cerr << "ERROR" << std::endl;
  std::cerr << msg << std::endl; //also prints a message since 
                                 //ERRORs without informations are a pain
  exit(1);
}


/**
 * \brief Make an int cell.
 * \param i The initial int value to be stored in the new cell.
 */
inline Cell* make_int(int i) {

  Cell* integerCell = (Cell*) malloc(sizeof(struct Cell));

  integerCell->tag_m = type_int;
  integerCell->int_m = i;

  return integerCell;

}

/**
 * \brief Make a double cell.
 * \param d The initial double value to be stored in the new cell.
 */
inline Cell* make_double(double d) {

  struct Cell* doubleCell = (Cell*) malloc(sizeof(struct Cell));
  
  doubleCell->tag_m = type_double;
  doubleCell->double_m = d;

  return doubleCell;

}

/**
 * \brief Make a symbol cell.
 * \param s The initial symbol name to be stored in the new cell.
 */
inline Cell* make_symbol(const char* s) {
  
  Cell* symbolCell = (Cell*) malloc(sizeof(struct Cell));

  //Make a deepcopy of the parameter value to ensure that the user's variable
  //will be untouched
  char* str_cpy = (char*) malloc(string_lenght(s));
  deep_string_copy(str_cpy, s);

  symbolCell->tag_m = type_symbol;
  symbolCell->symbol_m = str_cpy;

  return symbolCell;

}


/**
 * \brief Make a linked list node.
 * \param my_elem Pointer to the element to be held by this node.
 * \param my_next Pointer to the next node.
 */
inline Node* make_node(Cell* my_elem, Node* my_next) {
  
  if(my_elem == NULL)
    err("NullPointer: Cell not declared");

  // Although "kind of" constructor is available, Node will be created
  // with old fashion C style

  Node* newNode = (Node*) malloc(sizeof(struct Node));
  newNode->elem_m = my_elem;
  newNode->next_m = my_next;

  return newNode;

}

/**
 * \brief Check if d points to an int node.
 * \return True iff d points to an int node.
 */
inline bool intp(const Cell* c) {

  if(c == NULL) 
    err("NullPointer: Cell not declared");
  return c->tag_m == type_int;

}

/**
 * \brief Check if d points to a double node.
 * \return True iff d points to a double node.
 */
inline bool doublep(const Cell* c) {
  
  if(c == NULL)
    err("NullPointer: Cell not declared");

  return c->tag_m == type_double;

}

/**
 * \brief Check if d points to a symbol node.
 * \return True iff d points to a symbol node.
 */
inline bool symbolp(const Cell* c)
{
  if(c == NULL)
    err("NullPointer: Cell not declared");

  return c->tag_m == type_symbol;
}

/**
 * \brief Accessor (error if d is not an int node).
 * \return The value in the int node pointed to by c.
 */
inline int get_int(const Cell* c) {
  
  if(c == NULL)
    err("NullPointer: Cell not declared");

  if(c->tag_m != type_int)
    err("WrongTypeError: Cell does not contain an integer");
  
  return c->int_m;

}

/**
 * \brief Accessor (error if d is not a double node).
 * \return The value in the double node pointed to by c.
 */
inline double get_double(const Cell* c) {

  if(c == NULL)
    err("NullPointer: Cell not declared");
  
  if(c->tag_m != type_double)
    err("WrongTypeError: Cell does not contain a double");
  
  return c->double_m;

}

/**
 * \brief Retrieve the symbol name as a string (error if d is not a
 * symbol node).
 * \return The symbol name in the symbol node pointed to by c.
 */
inline char* get_symbol(const Cell* c) {

  if(c == NULL)
    err("NullPointer: Cell not declared");
  
  if(c->tag_m != type_symbol)
    err("WrongTypeError: Cell does not contain a string");
  
  return c->symbol_m;

}

/**
 * \brief Accessor.
 * \return The elem pointer in the linked list node pointed to by c.
 */
inline Cell* get_elem(const Node* n) {
  
  if(n == NULL)
    err("NullPointer: Node not declared");

  return n->elem_m;
}

/**
 * \brief Accessor.
 * \return The next pointer in the linked list node pointed to by c.
 */
inline Node* get_next(const Node* n) {

  if(n == NULL)
    err("NullPointer: Node not declared");  

  return n->next_m;
}

/**
 * \brief Print the linked list rooted at n in parentheses.
 * \param os The output stream to print to.
 * \param n The root node of the linked list to be printed.
 */
inline std::ostream& operator<<(std::ostream& os, const Node& n) {

  const Node* curr = &n;

  os << "(";

  while(curr != NULL) {

    Cell* c = get_elem(curr);

    if (intp(c)) {
      int value = c->int_m;
      os << value;
    }
    else if (doublep(c)) {
      double value = c->double_m;  
      os << value;
    }
    else if (symbolp(c)) {
      char* value = c->symbol_m;
      os << value;
    }
    
    if (get_next(curr)) {
      os << " ";
    }

    curr = get_next(curr);

  }

  os << ")" << std::endl;

}

#endif // LINKEDLIST_INTERNALS_HPP
