#include "linkedlist.hpp"

int list_size(const Node* n) {
  int counter = 0;
  while (n != NULL) {
    n = n->next_m;
    ++counter;  
  }

  return counter;  
}

Cell* list_ith(Node* n, unsigned int i) {
  if (i > list_size(n)-1)
    err("OutOfBoundError: position requested is bigger than the linkedlist's size");

  while (i-- > 0) {
    n = get_next(n);
  }
  return get_elem(n);
}


/*
  Implementation Approach:
   - copy subsequent node after pos (if any) into pos
   - delete subsequent node
  
  This approach enables us to also delete the head 
 */
Node* list_erase(Node* n, Node* pos) {
  if (pos == NULL) {
    err("Nullpointer Error! Are you trying to delete the tail/ the null terminator?");
  }

  while (n != pos) {
    n = get_next(n);
  }

  free(get_elem(n));
  
  Node* nextNode = get_next(n);
  
  if(nextNode != NULL) {
    free(n);
    n = make_node(get_elem(nextNode), get_next(nextNode));
    
    free(nextNode);
    return n;
  }
  else {
    return NULL;
  }
}


/* 
   Implementation approach:

   1. Create a new node with the value of the old head (1) and pointing to the same next node as the old head (2).
   2. Update the value of the old head to the value to be inserted (0).
   3. Update the next pointer of the old head to the address of the new node created in step 1.

   Credits to Karteek ADDANKI 
 */

Node* list_insert(Node* n, Node* pos, Cell* c) {
  Node* previousNode = NULL;

  while(n != pos) {
    previousNode = n;
    n = get_next(n);
  }

  if (n == NULL) { 
    
    // Just append new node to the linkedlist
    Node* newNode = make_node(c, NULL);
    Cell* previousCell = get_elem(previousNode);

    // No setter function in design specification, therefore node will
    // be replaced
    free(previousNode);
    previousNode = make_node(previousCell, newNode); 
    
    return newNode;

  }
  else {

    Node* nodeCopy =  make_node(get_elem(n), get_next(n));
   
    // No setter function -> just replace the Node
    free(n);
    n = make_node(c, nodeCopy);

    return n; 
  }
}

Node* list_insert_int(Node* n, Node* pos, const int value) {
  return list_insert(n, pos, make_int(value));
}

Node* list_insert_double(Node* n, Node* pos, const double value) {
  return list_insert(n, pos, make_double(value));
}

Node* list_insert_symbol(Node* n, Node* pos, const char* value) {
  return list_insert(n, pos, make_symbol(value));
}
