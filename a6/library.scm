(define > (lambda (x y) (< y x)))
(define >= (lambda (x y) (not (< x y))))
(define <= (lambda (x y) (not (< y x))))
(define = (lambda (x y) (if (< x y) 0 (not (< y x)))))
(define abs (lambda (x) (if (< x 0) (- 0 x) x)))
(define factorial (lambda (x) (if (< x 2) 1 (* x (factorial (- x 1))))))
(define nil (quote ()))
(define comment 
  (lambda args (print args)))
(define for-each
  (lambda (func list) 
    (func (car list)) 
    (if (not (null? (cdr list))) 
	(for-each func (cdr list)) 
	(quote ()))))
(define length
  (lambda (list)
    (if (null? list)
	0
	(+ 1 (length (cdr list))))))
(define first (lambda args (car args)))
(define square (lambda (x) (print (* x x))))
(define swap2 
  (lambda (list)
    (cons (car (cdr list)) (cons (car list) (quote ())))))
