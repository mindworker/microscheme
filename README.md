Microscheme - Scheme-Interpreter
================================

Implementation of a basic *turing-complete* Scheme interpretation, complying to these specs: https://mitpress.mit.edu/sicp/full-text/book/book-Z-H-4.html

To reach turing-completness, C++ is used as bootstrap medium. After I finished creating my 'own scheme', I continued to extend the language with it's own syntax, which can be found in `library.scm`

This project was created during the Honors course for OOP and Data Structures (COMP 2012H) at the Hong Kong University of Science and Technology, instructed by [Prof. Dekai Wu](http://www.cse.ust.hk/~dekai/) and Karteek Addanki.